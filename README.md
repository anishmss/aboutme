# Welcome!
## About me
I am a biological computerologist working on problems in computational biology.
I maintain a home page at [www.a-transposable-element.com](www.a-transposable-element.com).
The following are Bitbucket repositories of some projects I am/have been involved in: 



* Samar: speedy, assembly-free differential gene expression analysis for non-model organisms https://bitbucket.org/project_samar/samar

* Pipeline for 2-factor RNA-seq based gene expression analysis (as applied to S. serrata data)
https://bitbucket.org/s_serrata/dge

* Split-alignments of paired-end reads:
https://bitbucket.org/splitpairedend/last-split-pe/wiki/Home

* Joint read alignment:
https://bitbucket.org/jointreadalignment/jra-src/wiki/Home

* Handling a large sample of RNA secondary structures using binary decision diagrams:
https://bitbucket.org/rssdd/rssdd/wiki/Home
Unpublished